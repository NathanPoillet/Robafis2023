import React,{useState, useEffect,useRef} from "react";
import { Text, View, AppRegistry, Button, Pressable ,TextInput,FlatList} from "react-native";
import styles from '../../styles';
import SelectDropdown from 'react-native-select-dropdown';


// MEMOISE DROPDOWN POUR AUGMENTER VITESSE FLATLIST


const Dropdown=({refDropdown,setRefDropdown,data,setData,etat,item,dropdownData,toChange,nextDropdown,autotest})=>{
    //const dropdownRef1 = useRef({});  
    const [selected,setSelected]=useState(false);

    const checkIfUsable = () =>{
      if(data?.[item]!==undefined){
        return true;
      } else {
        return false;
      }
    }
    useEffect(()=>{
      setSelected(checkIfUsable());
    },[data])
    let disabled= autotest ? true : (!etat || (etat && !selected ) )
  
    return <SelectDropdown
      buttonStyle={[styles.buttonStyle, etat && selected ? (data?.[item]?.[toChange]!==undefined && data?.[item]?.[toChange]!==" "  ?  {backgroundColor:"rgba(201, 242, 155,1)"} : {backgroundColor:"rgba(255, 242, 155,1)"}) : etat  ? {backgroundColor:"#DCDCDC"}  : {backgroundColor:"rgba(236, 217, 221, 1)"}]}
      rowStyle={{flex:0}}
      ref={refDropdown}  
      defaultButtonText={(data?.[item]?.[toChange]!==undefined && data?.[item]?.[toChange]!==" "  ) ? data?.[item]?.[toChange] : "_"}
      defaultValue={(data?.[item]?.[toChange]!==undefined  && data?.[item]?.[toChange]!==" " ) ? data?.[item]?.[toChange]  : "_"}
      
      disabled={disabled}
      data={dropdownData}
      onSelect={(selectedItem, index) => {
        setData({...data,[item]: {...data[item],[toChange]:selectedItem}});
        
        nextDropdown?.current?.openDropdown();
      
      }}
      buttonTextAfterSelection={(selectedItem, index) => {
          if(selectedItem===" "){
            refDropdown.current.reset();
          }
          return selectedItem;
          }}
      rowTextForSelection={(item, index) => {
          return item;
      }}
    />
}
export default Dropdown;