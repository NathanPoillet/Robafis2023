import React,{useState, useEffect,useRef} from "react";
import { Text, View, AppRegistry, Button, Pressable ,TextInput,FlatList} from "react-native";
import styles from '../../styles';

const ValeurLue=({item,etat,data,setData,dropdown1,dropdown2,dropdown3,autotest})=>{

    const [select,setSelect] = useState((Object.keys(data).length === 0 || data?.[item]) ? false : true);
    const [lastData,setLastData] =  useState({"aEcrire":" ","deplacement":" ","etatSuivant":" "});

    const ChangeSelection=(ligne)=>{

        if(!select){
          setData({...data,[item]:lastData});
          if(lastData.aEcrire===" "  ){
            dropdown1?.current?.openDropdown();
          } else if(lastData.deplacement===" "){
            dropdown2?.current?.openDropdown();
          } else if(lastData.etatSuivant===" " ){
            dropdown3?.current?.openDropdown();
          }
        } else {
          let dataTmp = { ...data };
          setLastData(lastDatadata => ({...dataTmp[ligne]}));
          delete dataTmp[ligne];
          setData(data => ({...dataTmp}));
        }
        setSelect(!select);
    }

    useEffect(()=>{
      setSelect((Object.keys(data).length === 0 || data?.[item]=== undefined) ? false : true);
    },[data])

    return  <Pressable style={[styles.buttonVersOu, (etat && select) ? {backgroundColor:"rgba(201, 242, 155,1)"} : etat && {backgroundColor:"#DCDCDC"}]}
    onPress={()=>ChangeSelection(item)} disabled={autotest}>
        <Text>{item}</Text>
    </Pressable> 
}

export default ValeurLue;