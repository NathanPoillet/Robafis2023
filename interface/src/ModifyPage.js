import { Text, View, AppRegistry, Button, Pressable ,TextInput,FlatList, ScrollView,Alert} from "react-native";
import ItemModify from './ItemModify';
import styles from '../styles';
import {  Link,useParams,useLocation } from "react-router-native";
import React,{useState,useRef, useEffect, useCallback,Suspense,lazy} from "react";
import Ligne from "./Ligne";
import AsyncStorage from '@react-native-async-storage/async-storage';

import { useNavigate } from "react-router-native";
import DialogInput from 'react-native-dialog-input';
import AnimatedLoader from 'react-native-animated-loader';

/**
 * Enregistre un programme dans l'espace de stockage de l'application
 * @param {*} key clé pour accéder au programme
 * @param {*} data données du programme
 */
const _storeData = async (key,data) => {
  try {
    await AsyncStorage.setItem(
      key,
      data,
    );
  } catch (error) {
    // Error saving data
    console.log(error);
  }
};


/**
 * 
 * @returns Page pour modifier ou ajouter un programme
 */

const ModifierOuAjouterProgramme = ({setMenu}) => {

  //Obtiens l'url de la page (savoir si on modifie une page oupa)
  const location = useLocation();

  //initialise le hook pour naviguer entre les pages de manière programmatique
  const navigate = useNavigate();

  // nom du programme à ajouter ou modifier
  const [nomData,setNomData]=useState("");

  //données de chaque état du programme
  const [dataInit,setDataInit] = useState({});
  const [dataE1,setDataE1] = useState({});
  const [dataE2,setDataE2] = useState({});
  const [dataE3,setDataE3] = useState({});
  const [dataE4,setDataE4] = useState({});

  //inutile
  const [enregistrer,setEnregistrer] = useState(false);



  // A chaque rerender, va chercher les données du programme à modifier (si location.state n'est pas null, ie si on modifie un programme déjà existant).
  // si la key est null, alors on ne fait rien (ajout d'un nouveau programme)
  useEffect(()=>{

    //création de la fonction pour chercher les données
    _retrieveData = (key) => {
      try {

        setTimeout(async ()=>{
          if(key!==null && key!=="autotest"){
            const value = await AsyncStorage.getItem(key);
            if (value !== null) {
              console.log(value);
              setDataInit(JSON.parse(value)["Init"]);
              setDataE1(JSON.parse(value)["e1"]);
              setDataE2(JSON.parse(value)["e2"]);
              setDataE3(JSON.parse(value)["e3"]);
              setDataE4(JSON.parse(value)["e4"]);
              setNomData(key);
            }
          } else if(key==="autotest"){
              setDataInit({"Vide":{"aEcrire":"Vide","deplacement":"droite","etatSuivant":"e1"}});
              setDataE1({"Vide":{"aEcrire":"1","deplacement":"gauche","etatSuivant":"e2"},"0":{"aEcrire":"0","deplacement":"droite","etatSuivant":"e1"},"1":{"aEcrire":"0","deplacement":"droite","etatSuivant":"e1"}});
              setDataE2({"Vide":{"aEcrire":"1","deplacement":"droite","etatSuivant":"fin"},"0":{"aEcrire":"0","deplacement":"gauche","etatSuivant":"e2"}});
              setDataE3({});
              setDataE4({});
              setNomData(key);
              console.log("autotest");
          }
        },500)
       
      } catch (error) {
        // Error retrieving data
        console.log(error);
      }
    };

    //exécution au rerender
    _retrieveData(location.state);
    setMenu(-1);
  },[])



  /**
   * Enregistre le programme enter dans l'espace de stockage de l'application
   */
  const enregistrerButton = async ()=> {
    setEnregistrer(true);

    if(nomData!=="" && nomData!==undefined){
      console.log("enregistrer");
  
      // console.log({nom:nomData,Init:dataInit,e1:dataE1,e2:dataE2,e3:dataE3,e4:dataE4});
  
      await _storeData(nomData,JSON.stringify({nom:nomData,Init:dataInit,e1:dataE1,e2:dataE2,e3:dataE3,e4:dataE4}));
      navigate('/tousPrograms');
    }
  }

  const dialogEnregistrer = async (nom)=>{
    if(nom!=="" && nom!==undefined){
      console.log("enregistrer");
  
      //console.log({nom:nomData,Init:dataInit,e1:dataE1,e2:dataE2,e3:dataE3,e4:dataE4});
  
      await _storeData(nom,JSON.stringify({nom:nom,Init:dataInit,e1:dataE1,e2:dataE2,e3:dataE3,e4:dataE4}));
      navigate('/tousPrograms');
    }
  }




  /**
   * 
   * @param {*} props component pour le FlatList avec les différentes lignes
   * @returns 
   */
  const renderLigne =(props)=>{
    switch(props.index){
      case 0 :
        return renderLigneInit(props);
      case 1: 
        return renderLigneE1(props);
      case 2: 
        return renderLigneE2(props);
      case 3: 
        return renderLigneE3(props); 
      case 4: 
        return renderLigneE4(props);
      default:
        return renderLigneInit(props);
    }
  }
  // const renderLigne1 =(props)=>{
  //   switch(props.index){
  //     case 0 :
  //       return renderLigneInit1(props);
  //     case 1: 
  //       return renderLigneE11(props);
  //     case 2: 
  //       return renderLigneE21(props);
  //     case 3: 
  //       return renderLigneE31(props); 
  //     case 4: 
  //       return renderLigneE41(props);
  //     default:
  //       return renderLigneInit1(props);
  //   }
  // }

  // const renderLigneInit1 =({item})=><Ligne item={item.nom} data={dataInit} setData={setDataInit} autotest={location.state==="autotest"} />;
  // const renderLigneE11 = ({item})=><Ligne item={item.nom} data={dataE1} setData={setDataE1} autotest={location.state==="autotest"}/>;
  // const renderLigneE21 = ({item})=><Ligne item={item.nom} data={dataE2} setData={setDataE2} autotest={location.state==="autotest"} />;
  // const renderLigneE31 = ({item})=><Ligne item={item.nom} data={dataE3} setData={setDataE3} autotest={location.state==="autotest"}/>;
  // const renderLigneE41 = ({item})=><Ligne item={item.nom} data={dataE4} setData={setDataE4} autotest={location.state==="autotest"}/>;

  const renderLigneInit = useCallback(({item})=><Ligne item={item.nom} data={dataInit} setData={setDataInit} autotest={location.state==="autotest"} />,[dataInit]);
  const renderLigneE1 = useCallback(({item})=><Ligne item={item.nom} data={dataE1} setData={setDataE1} autotest={location.state==="autotest"}/>,[dataE1]);
  const renderLigneE2 = useCallback(({item})=><Ligne item={item.nom} data={dataE2} setData={setDataE2} autotest={location.state==="autotest"} />,[dataE2]);
  const renderLigneE3 = useCallback(({item})=><Ligne item={item.nom} data={dataE3} setData={setDataE3} autotest={location.state==="autotest"}/>,[dataE3]);
  const renderLigneE4 = useCallback(({item})=><Ligne item={item.nom} data={dataE4} setData={setDataE4} autotest={location.state==="autotest"}/>,[dataE4]);
  const keyExtractor=useCallback((item)=>item.id,[]);
  const [loading, setLoading] = useState(true);
 

  useEffect(()=>{
    console.log(loading);
    setTimeout(() => {
      setLoading(false);
  }, 500);
  },[])


return   <View>
{loading ? <View style={{height:"85%",marginLeft:"7.5%"}}><AnimatedLoader
visible={true}
source={require("../assets/loading.json")}
overlayColor="rgba(255,255,255,0.75)"
animationStyle={{width:100,height:100}}
speed={1}>
  <Text>Doing something...</Text>
</AnimatedLoader></View> : 
<ScrollView style={{height:"85%",marginLeft:"7.5%"}} contentContainerStyle={{}}>
    <View style={{ display:"flex",flexDirection:"column"}}>   
 
      <View style={{ display:'flex',flexDirection:"row"}}>
        <View style={{width:"15%",display:"flex",alignItems:"center"}}>
          <Text>Etat</Text>
        </View>
        <View style={{width:"20%",display:"flex",alignItems:"center"}}>
          <Text>Valeur lue</Text>
        </View>
        <View style={{width:"20%",display:"flex",alignItems:"center"}}>
          <Text>A écrire</Text>
        </View>
        <View style={{width:"20%",display:"flex",alignItems:"center"}}>
          <Text>Déplacement</Text>
        </View>
        <View style={{width:"15%",display:"flex",alignItems:"center"}}>
          <Text>Etat suivant</Text>
        </View>
      </View>

      <View >
      
        <FlatList    
          data={[{nom:"Init",id:0},{nom:"e1",id:1},{nom:"e2",id:2},{nom:"e3",id:3},{nom:"e4",id:4}]}
          renderItem={renderLigne}
          keyExtractor={keyExtractor}
          scrollEnabled={false}
          initialNumToRender={5}
          maxToRenderPerBatch={4}
          windowSize={2}
        />
       
      </View>

      {location.state!=="autotest" && 
        <View style={{display:"flex",alignItems:"center",justifyContent:"center",marginTop:10,marginRight:"10%"}}> 
          <Pressable style={[styles.button2]} onPress={()=>{enregistrerButton()}}><Text style={styles.textButton2}>Enregistrer</Text>
          </Pressable>
        </View>
      }
      <DialogInput 
        isDialogVisible={enregistrer && (nomData==="" || nomData===undefined)}
        title={"Ajouter un nom"}
        message={"Entrer un nom de programme : "}
        hintInput ={"nom..."}
        submitInput={ (text) => {dialogEnregistrer(text)}}
        cancelText={"Annuler"}
        submitText={"Enregistrer"}
        closeDialog={ () => {setEnregistrer(false)}}>
        
      </DialogInput>
    
    </View>
  </ScrollView>}</View>
  }
  


const ModifierPage = ()=> <View style={{height:"85%",marginLeft:"7.5%"}}>  
<AnimatedLoader
visible={true}
source={require("../assets/loading.json")}
overlayColor="rgba(255,255,255,0.75)"
animationStyle={{width:100,height:100}}
speed={1}>
  <Text>Doing something...</Text>
</AnimatedLoader>
</View> 
;

const Test =() => <Text>salut</Text>;

export default ModifierOuAjouterProgramme;