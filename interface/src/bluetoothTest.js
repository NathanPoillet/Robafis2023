import { useEffect,useState,useRef } from 'react';

import { Pressable, View,Text, TextInput, ScrollView } from 'react-native';
import { PermissionsAndroid } from 'react-native';
import {decode as atob, encode as btoa} from 'base-64'
import AsyncStorage from '@react-native-async-storage/async-storage';


import {encode,encodeDeplacement,encodeEcrire,encodeEtatSuivant,decodeAbsenceInstruction,decodeDeplacement,decodeErreurLecture,decodeEtat} from "./utils/encode"



const messageArduino ={
  0: "Erreur lors de l'envoie du message : Renvoie du message",
  1: "Etat courant : {v}",
  2: "Valeur lecture : {v} ",
  3: "Valeur écriture : {v} ",
  4: "Déplacement effectué : {v}",
  5: "Erreur lors de la lecture : un nombre incohérent de cubes a été détecté {0,h,2,e,default}",
  6: "Le programme s'est terminé.",
  7: "Une instruction est manquante (0,1,2)",
}

const infoColor="green";
const erreurColor="rgb(255,50,50)";


const autotest = "I235iVVdaa10daa00daaV1gbbV1dfb00gbE";

const BluetoothTest= ({connected,device,interruption,setInterruption,arrayBLE,setArrayBLE,run,setRun,setArrayResp,resp,programmeEnCours,setProgrammeEnCours,lastMessage,setLastMessage,etatCourant,setEtatCourant,setMenu})=>{

  const [programmes,setProgrammes]= useState([{nom:"autotest",programme:"I235iVVdaaV1gba00daa10dabV1dfb00gbE"}]);

  const scrollViewRef = useRef();
  const dropdown = useRef();

  const serviceUUID="0000ffe1-0000-1000-8000-00805f9b34fb";

  const characteristicWrite_UUID="0000ffe3-0000-1000-8000-00805f9b34fb";
  const characteristicRead_UUID="0000ffe2-0000-1000-8000-00805f9b34fb";
  const nomDevice=["Makeblock_LE703E9701994E","Makeblock_LE703E979314F7","Makeblock_LE703e97e30197"]

  //Obtiens l'url de la page
  const location = useLocation();



  const write = async (message)=> {
    console.log(message);
    if(nomDevice.includes(device?.name)){
      setLastMessage(message);
      device?.writeCharacteristicWithResponseForService(serviceUUID,characteristicWrite_UUID,btoa(message))
      .then(res=>console.log("message envoyé : " + message))
      .catch(error=> console.log(error));
    }
  }


  


  useEffect(()=>{
    _retrieveData = async (key) => {
      try {
      
        const value = await AsyncStorage.getItem(key);
        if (value !== null) {
          let program= JSON.parse(value);

          let header = "I2";

          let init = encode(program.Init,"i");
          let un = encode(program.e1,"a");
          let deux = encode(program.e2,"b");
          let trois = encode(program.e3,"c");
          let quatre = encode(program.e4,"d");

          let res = init + un + deux + trois + quatre;
          let size = res.length + 5;
          size = size<10 ? "0" + size : size;
          header = header + size;

          res= header + res + 'E';

          setProgrammes(programmes => [...programmes,{nom:program.nom,programme:res}]);

         

          // console.log([...programmes,{nom:program.nom,programme:res}])
        }
      } catch (error) {
        // Error retrieving data
        console.log(error);
      }
    };
    
    
    _getAllData = async () => {
      try {
        const keys = await AsyncStorage.getAllKeys().then((keys)=>{
          console.log("keys : " + keys.length);
          if(keys.length>programmes.length-1){
            keys.forEach(async (key)=>{
               _retrieveData(key);
            })
          }
        });
      } catch (error) {
        // Error retrieving data
        console.log(error);
    
      };
    }

    _getAllData();
    setMenu(2);

  },[])






  useEffect(()=>{

    console.log(programmes);
    console.log(programmeEnCours)
    
    if(location.state!==null){
      console.log("programmes : " + JSON.stringify(programmes));
      let programmeTemp = programmes.filter((a)=>a.nom===location.state).at(0);
      if(programmeTemp!==undefined && programmeEnCours.nom !==programmeTemp.nom){
        setProgrammeEnCours(programmeTemp);
        let index = programmes.findIndex((element)=>element.nom===programmeTemp.nom);
        dropdown.current.selectIndex(index);
      }

    } else {
      console.log(" en cours " + JSON.stringify(programmeEnCours));

      let programmeTemp = programmes.filter((a)=>a.nom===programmeEnCours.nom).at(0);
      if(programmeTemp) setProgrammeEnCours(programmeTemp);
      
      let index = programmes.findIndex((element)=>element.nom===programmeEnCours?.nom);

      dropdown.current.selectIndex(index);
   }

  },[programmes])





  useEffect(()=>{

    console.log("programme en cours : " + JSON.stringify(programmeEnCours));

  },[programmeEnCours])




  const RunButton =()=>{
    write(programmeEnCours.programme);
    console.log(" interruptio ndans run ?" +interruption)

    if(interruption){
      console.log("here")

      setArrayBLE([...arrayBLE,{text:"-------------------",who:0},{text:"Lancement de " + programmeEnCours.nom,who:0}]);
      setInterruption(false);
    } else {
      setArrayBLE([...arrayBLE,{text:"-------------------",who:0},{text:"Lancement de " + programmeEnCours.nom,who:0}]) ;
    }
  }




  const gereInterruption = () => {
    if(interruption){
      write("I105E");
      setArrayBLE([...arrayBLE,{text:"Reprise du programme...",who:0}]);
    } else{
      write("I005E");
      setArrayBLE([...arrayBLE,{text:"Interruption du programme.",who:0}]);
    }
    setInterruption(!interruption);
  }



  const Reinitialiser = ()=>{
    console.log("reinit");
    setRun(false);
    setInterruption(false);
    setArrayBLE([...arrayBLE,{text:"Arrêt du programme. Réinitialisation.",who:0}]);


  }


  const [arretDurgence,setArretDurgence] =useState(!connected || !run);
  const [envoyer,setEnvoyer] =useState(!interruption && (run || !connected));
  const [reinit,setReinit] =useState(!interruption); 
  const [dropdownDisabled,setDropdownDisabled] = useState(!run);
  

  useEffect(()=>{

    setArretDurgence(!connected || !run);
    setEnvoyer(!interruption && (run || !connected));
    setReinit(!interruption);
    setDropdownDisabled( !interruption && run);

  },[interruption,connected,run])






  return <View style={{justifyContent:'center',alignItems:"center",height:"85%"}}>
        
    <View style={{height:"85%",width:"80%",backgroundColor:"rgba(250, 250, 250, 1)",marginLeft:"10%"
    ,elevation:10,borderRadius:15,marginRight:"10%",borderWidth:4,borderColor:"rgba(6, 148, 105, 1)"}}>
      
    
      <View style={{borderWidth:10,borderRadius:10,borderColor:"rgb(40, 40, 40)",overflow:"hidden",elevation:10}}>
        <View style={{display:'flex',height:"4%",flexDirection:"row",paddingRight:10,justifyContent:"space-between",backgroundColor:"rgb(40, 40, 40)"}}>

          <View style={{display:'flex',flexDirection:"row",paddingRight:10,justifyContent:'flex-start'}}>
            <View style={{backgroundColor:"green",height:"30%",width:10,maxHeight:5,borderRadius:20,marginRight:10}}/>
            <View style={{backgroundColor:"yellow",height:"30%",width:10,maxHeight:5,borderRadius:20,marginRight:10}}/>
            <View style={{backgroundColor:"red",height:"30%",width:10,maxHeight:5,borderRadius:20}}/>
          </View>

        </View>

        <View style={{height:"96%",padding:5,borderWidth:5,borderColor:"rgba(0, 0, 0, 0.1)"}}>
        <View style={{display:"flex",flexDirection:"row",justifyContent:'center',alignItems:'center',borderColor:"black"}}>
              <View  >
                <Text style={{marginBottom:-5}}>
                {"    ______         __   _       ____       "}   
                </Text> 
                <Text style={{marginBottom:-5}}>
                {"  /  ___\\   \\     /  / /  \\    /    __|( )    "}   
                </Text > 
                <Text style={{marginBottom:-5}}>
                {" |   |       \\   \\_/  / / _  \\  |    |__ __   ___ "}             
                </Text>  
                <Text style={{marginBottom:-5}}>
                {" |   |         \\      / /  / \\  \\ |     __|   | / __|"}              
                </Text>
                <Text style={{marginBottom:-5,marginLeft:-2}}>
                {" |   |____    |    |/   ___    \\    |   |    |\\__ \\"}             
                </Text>
                <Text style={{marginBottom:-5,marginLeft:-2}}>
                {"  \\______|  |__ |  /       \\__\\_|   |__| |___/"}              
                </Text>

                <Text style={{marginBottom:"10%"}}>
            
                </Text>
              </View>
            </View>
        <ScrollView  contentContainerStyle={{flexGrow:1,justifyContent:"flex-end"}}  
          ref={scrollViewRef}
          onContentSizeChange={() => scrollViewRef.current.scrollToEnd({ animated: true })}>
          <View style={{display:"flex",flexDirection:"column",justifyContent:"center",paddingBottom:30,paddingLeft:10,width:"84%"}}>
            
            
            
            {arrayBLE.map((item,index) => <View key={index} style={{display:"flex",flexDirection:"row",width:"100%"}}>
              <Text style={item.who===0 ? {color:"rgba(9, 130, 130, 1)"} : {color:"rgba(255, 121, 147, 1)",width:"30%"}}>{item.who===0 ? "robafis@IHM : ": "robafis@CyAfis:~ "  } </Text>
              {item.color && <Text style={{fontFamily:"Monospace",color: item.color,fontWeight: "bold",maxWidth:"15%"}}> {item.color===infoColor ? "[Info] " : "[Erreur] "}</Text>}
              <Text style={{fontFamily:"Monospace",color:"rgb(40, 40, 40)",width: item.color ? "55%" : "70%"}}>{item.text}</Text>
              </View>)
              }
             
          </View>
        </ScrollView>
        <View  style={{display:"flex",flexDirection:"row",marginTop:10,padding:5,borderWidth:1,borderColor:"rgb(230, 230, 230)",backgroundColor:"rgba(245, 245, 245, 1)",alignItems:"center"}}>
            <Text style={ {color:"rgba(10, 140, 140, 1)",width:"20%"} }>{"robafis@IHM : "  } </Text>

            <SelectDropdown
            ref={dropdown}
            disabled={dropdownDisabled}
            buttonStyle={[styles.dropdown2,dropdownDisabled && {backgroundColor: "rgb(240,240,240)"}]}
            buttonTextStyle={{flex:1,fontSize:14}}
            rowStyle={{flex:1,fontSize:30}}
            // disabled={run || !connected}
            // defaultButtonText={location.state!==null ?  location.state : 'Charger programme...'}
            // defaultValue={location.state!==null ? location.state : "autotest"}
            data={programmes.map((a)=>a.nom)}
            onSelect={(selectedItem, index) => {
              console.log(selectedItem, index);
              setProgrammeEnCours(programmes.filter((a)=>a.nom===selectedItem).at(0));
            }}
            buttonTextAfterSelection={(selectedItem, index) => {
              return selectedItem;
            }}
            rowTextForSelection={(item, index) => {
              return item;
            }}
          />
          <View style={{position:'absolute',right:15,width:"30%",justifyContent:'center'}}>
            <Pressable style={({pressed}) => [styles.run,{backgroundColor:pressed ? "rgb(240,240,240)": "white"}, envoyer && {backgroundColor: "rgb(240,240,240)"}]}  disabled={envoyer} onPress={()=>{RunButton()}}>
              <Text> {"Envoyer"} </Text>
            </Pressable>
          </View>
        </View>
        </View>

      </View>
    </View>

    <View style={{display:'flex',flexDirection:"row",paddingLeft:10,paddingRight:10,alignItems: "center",marginTop:20,height:"10%"}}>
      
      <Pressable style={ [styles.buttonArret, arretDurgence && {backgroundColor: "rgb(240,240,240)"}]}  disabled={arretDurgence} onPress={()=>gereInterruption()}>
        <Text>{interruption ? "Reprise": "Arrêt d'urgence " } </Text>
        </Pressable>
    

      <Pressable style={[styles.buttonArret,reinit && {backgroundColor: "rgb(240,240,240)"}]} disabled={reinit} onPress={()=>{Reinitialiser()}}>
        <Text>Réinitialiser</Text>
      </Pressable>

    </View>
  </View>

}


import { StyleSheet } from "react-native";
import SelectDropdown from 'react-native-select-dropdown';
import { useLocation } from 'react-router-native';
const styles = StyleSheet.create({
  buttonConnect: {
    margin: 8,
    backgroundColor:"white",
    elevation: 2
  },
  buttonArret: {
    margin: 8,
    backgroundColor:"white",
    elevation: 2,
    height:"100%",
    width:"20%",
    justifyContent:"center",
    alignItems:"center",
    borderRadius:10
  },
  buttonRun:{
    margin: 8,
    backgroundColor:"red",
    elevation: 2,
    height:"100%",
    width:"20%",
    justifyContent:"center",
    alignItems:"center",
    borderRadius:10
  },
  dropdown:{
    width:"20%",
    elevation: 5,
    borderRadius:10,
    backgroundColor:"white",
    height:"100%",

  },
  dropdown2:{
    width:"40%",
    elevation: 5,
    borderRadius:10,
    backgroundColor:"white",
    marginRight:20,
    
  },
  run: {
    margin: 8,
    backgroundColor:"white",
    elevation: 2,
    height:"100%",
    width:"100%",
    justifyContent:"center",
    alignItems:"center",
    borderRadius:10,
  },
})

export default BluetoothTest;



