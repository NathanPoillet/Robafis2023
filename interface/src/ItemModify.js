import React,{useState,useRef, useEffect} from "react";
import { StyleSheet, Text, View, AppRegistry, Button, Pressable ,TextInput,Switch} from "react-native";

import { NativeRouter, Route, Routes, Link,useParams,useLocation } from "react-router-native";
import SelectDropdown from 'react-native-select-dropdown';

/*
Changement à faire :

- les programmes

Fonction d'enregistrement, où on enregistre les changement (si nouveau programme crée quelque chose de nouveau, sinon modifie.)
-> dans l'enregistrement, faire un sort sur le nom de l'état (ça pourrait changer si on fait nimp avec les lignes, genre effacant tout etc)
Fonction de décodage d'une array to un truc lisible par react native (en gros de data, afficher ça dans des drops down (pour modif))

Fonction d'encodage (mettre le tout en csv, et envoyer par bluetooth au arduino)



- communication robot

Fenêtre afficher état du robot (lorsque l'on a appuyé sur Run)
Commande arrêt d'urgence / Reprise
Possibilité de lancer un programme (envoie message au robot par bluetooth pour commencer exéc)

*/



const ItemModify = ({data,setData,indexLigne,state,data1,setData1})=>{
  const dropdownRef1 = useRef({});  
  const dropdownRef2 = useRef({});  
  const dropdownRef3 = useRef({});  
  const dropdownRef4 = useRef({});  
  const dropdownRef5 = useRef({});  

  console.log("modifier");

  const changerData =(indexArray,index,newItem) =>{
    const newData = data1.map((array, i) => {
      if (i === indexArray) {
        return array.map((item, j) => {
          if(j==index){
            return newItem;
          } else {
            return  item
          }
        })
      } else {
        return array;
      }
    });
    setData1(newData);
    console.log(newData);
  }  

  const deleteData= (indexArray) => {
    console.log(data.filter((_,i)=> i!== indexArray));

    if(data.length>0){
      setData1(data.filter((_,i)=> i!== indexArray));
      setData(data.filter((_,i)=> i!== indexArray));

    }
  }

    //~~(state / 3)===0 ? "init" : "e"+ ~~(state / 3)

  return     <View style={{ display:'flex',flexDirection:"row"}}>

     
     <SelectDropdown
        buttonStyle={styles.buttonStyle}
        rowStyle={{flex:0}}
         ref={dropdownRef1}  
        defaultButtonText='Choisissez une option'
        defaultValue={data1.at(indexLigne).at(0)}
        data={["","init","e1","e2","e3","e4"]}
        onSelect={(selectedItem, index) => {
          console.log(selectedItem, index);
          if(selectedItem!==""){
            dropdownRef2.current.openDropdown();
          }
          changerData(indexLigne,0,selectedItem);
        }}
        buttonTextAfterSelection={(selectedItem, index) => {
          if(selectedItem===""){
              dropdownRef1.current.reset();
            }
          return selectedItem;
        }}
        rowTextForSelection={(item, index) => {
          return item;
        }}
      />
       <SelectDropdown
        buttonStyle={styles.buttonStyle}
        rowStyle={{flex:0}}
        ref={dropdownRef2}  
        defaultButtonText='Choisissez une option'
        data={["","Vide","0","1"]}
        onSelect={(selectedItem, index) => {
          console.log(selectedItem, index);
          if(selectedItem!==""){
            dropdownRef3.current.openDropdown();
          }
          changerData(indexLigne,1,selectedItem);

        }}
        buttonTextAfterSelection={(selectedItem, index) => {
          if(selectedItem===""){
            dropdownRef3.current.reset();
          } 
          return selectedItem;
        }}
        rowTextForSelection={(item, index) => {
          return item;
        }}
      />
   <SelectDropdown
        buttonStyle={styles.buttonStyle}
        ref={dropdownRef3}  
        defaultButtonText='Choisissez une option'
        data={["","Vide","0","1"]}
        // defaultValueByIndex={1} // use default value by index or default value
        // defaultValue={'Canada'} // use default value by index or default value
        onSelect={(selectedItem, index) => {
          console.log(selectedItem, index);
          if(selectedItem!==""){
            dropdownRef4.current.openDropdown();
          }
          changerData(indexLigne,2,selectedItem);

        }}
        buttonTextAfterSelection={(selectedItem, index) => {
          if(selectedItem===""){
            dropdownRef3.current.reset();
          }
          return selectedItem;
        }}
        rowTextForSelection={(item, index) => {
          return item;
        }}
      />
        <SelectDropdown
        buttonStyle={styles.buttonStyle}
        ref={dropdownRef4}  
        defaultButtonText='Choisissez une option'
        data={["","droite","gauche"]}
        // defaultValueByIndex={1} // use default value by index or default value
        // defaultValue={'Canada'} // use default value by index or default value
        onSelect={(selectedItem, index) => {
          console.log(selectedItem, index);
          if(selectedItem!=="")
          {
            dropdownRef5.current.openDropdown();
          }
          changerData(indexLigne,3,selectedItem);
  
        }}
        buttonTextAfterSelection={(selectedItem, index) => {
          if(selectedItem===""){
            dropdownRef4.current.reset();
          }
          return selectedItem;
        }}
        rowTextForSelection={(item, index) => {
          return item;
        }}
      />
      <SelectDropdown
        buttonStyle={styles.buttonStyle}
        ref={dropdownRef5}  
        defaultButtonText='Choisissez une option'
        data={["","init","e1","e2","e3","e4","fin"]}
        // defaultValueByIndex={1} // use default value by index or default value
        // defaultValue={'Canada'} // use default value by index or default value
        onSelect={(selectedItem, index) => {
          changerData(indexLigne,4,selectedItem);
          console.log(selectedItem, index);
        }}
        buttonTextAfterSelection={(selectedItem, index) => {
          if(selectedItem===""){
            dropdownRef5.current.reset();
          }
          return selectedItem;
        }}
        rowTextForSelection={(item, index) => {
          return item;
        }}
      />  
      <Pressable style={{display:"flex",justifyContent:"center",alignItems:"center", marginLeft:"10px", backgroundColor:"red" }}
      onPress={()=>deleteData(indexLigne)}>
        <Text>Delete</Text>
      </Pressable>

  </View>
  }


  const styles = StyleSheet.create({
    buttonStyle: {
      width:"20%",
      height:40,
      marginLeft:2,
      borderColor:"black",
      borderWidth:1
  
    },
    page:{
      height:"80%",
    }
  })

  export default  ItemModify;