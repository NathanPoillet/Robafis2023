// AJOUTER UN BOUTON DELETE
// RENDRE LE BOUTON RUN FONCTIONEL
// A faire : Mettre un bouton effacer sur les listes des programmes

import {  Text, View, AppRegistry, Button, Pressable ,TextInput,FlatList, ScrollView,Image} from "react-native";
import {  Link,useParams,useLocation } from "react-router-native";

import styles from '../styles';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useEffect, useState } from "react";


const AllProgram = ({setMenu}) =>{

    const [programmes,setProgrammes]=useState([]);
    const [loading,setLoading]=useState(false);


    _deleteData = async (key)=>{
      try {
            
        AsyncStorage.removeItem(key).then(()=>{
          console.log(programmes);
          const progTemp = programmes.filter((a)=>a.nom!==key);
          console.log(progTemp);
          setProgrammes(progTemp);
        })
        .catch(e => console.log(e));
       } catch (error) {
        // Error retrieving data
        console.log(error);
      }
    }


      useEffect( ()=>{

        _retrieveData = async (key) => {
            try {
             
              const value = await AsyncStorage.getItem(key);
              if (value !== null) {
                // We have data!!
                console.log(value);
                setProgrammes(programmes => [...programmes,JSON.parse(value)]);
              }
            } catch (error) {
              // Error retrieving data
              console.log(error);
            }
          };
          
          
          _getAllData = async () => {
            try {
              const keys = await AsyncStorage.getAllKeys().then((keys)=>{
                console.log(keys);
                if(keys.length!==programmes.length){
                  keys.forEach(async (key)=>{
                    _retrieveData(key);
                  })
                }
              })
            } catch (error) {
              // Error retrieving data
              console.log(error);
          
            };
        }
         _getAllData();

         setMenu(1);
       

      },[])



    return <View style={styles.page}>
      <View style={{marginTop:"50px", display:'flex', alignItems:"center",height:"100%"}}>
      <Text>Liste des programmes</Text>

        <Item name={"autotest"} id ={"autotest"} autotest={true} setLoading={setLoading}/>

        <View style={{display:"flex",flexDirection:"row",height:"60%"}}>


        <FlatList
        data={programmes}
        renderItem={({item,index})=><Item name={item.nom} id ={item.nom + index} autotest={false} setLoading={setLoading}/>}
        keyExtractor={item => item.nom}
        />
          </View>

      <View style={{display:"flex",flexDirection:"row",justifyContent:"center",alignItems:"center", height:"40%"}}>
    
    <Link to="/newProgram" style={styles.button1}>
        <Text style={styles.textButton}>Ajouter un programme</Text>
    </Link>
  </View> 
      </View>
    </View>
}
  
const Item =({id,name,autotest,setLoading})=> {

  console.log("item : " + id + " " + name)
    return  <View style={{display:"flex",flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
      <View style={{width:"70%",flexDirection:"row",alignItems:"center",justifyContent:"center"}}>

      {!autotest  ? <Pressable style={{alignItems:"center",justifyContent:"center",borderWidth:1,height:50,width:50,borderRadius:10,backgroundColor:"rgb(255,50,50)"}}
        onPress={()=>_deleteData(name)}>
          <Image style={{width:30,height:30}} source={require("../corbeille.png")}/>
        </Pressable>
        :
        <View  style={{height:50,width:50}}/>
        }
        <Link to={"/TestBluetooth/" + id} state={name} style={styles.buttonRun}>
          <Text style={styles.textButton2}>Run</Text>
        </Link>
        <Link to={"/modifyProgram/" + id} state={name} style={styles.buttonAcceder} onPress={()=>setLoading(true)}> 
          <Text style={styles.textButton}>{autotest ? "Accéder" : "Modifier"}</Text>
        </Link>
        
        <Text  style={styles.textProgramme}>{name}</Text>
        </View>

    </View>
}

export default AllProgram;
