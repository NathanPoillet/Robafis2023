
import { Text, View, AppRegistry, Button, Pressable ,TextInput,FlatList} from "react-native";
import styles from '../styles';
import {  Link,useLocation } from "react-router-native";
import React,{useState, useEffect,useRef} from "react";

import ValeurLue from "./Programmes/ValeurLue";
import Dropdown from "./Programmes/Dropdown";

const Ligne = React.memo(({item,data,setData,autotest}) => {
  // console.log(item);
  const [etat,setEtat] = useState(false);
  const dropdownRefList = [useRef(1),useRef(2),useRef(3),useRef(4),useRef(5),useRef(6),useRef(7),useRef(8),useRef(9)];

  const [data1,setData1]=useState({});


  useEffect(()=>{
    if(etat){
      setData(data1);
    }
  },[data1])

  useEffect(()=>{
    if(!etat){
      setData({})
    } else {
      setData(data1)
    }
  },[etat])


  useEffect(()=>{

    if(Object.keys(data).length !== 0 && !etat){
      setEtat(true);
      setData1(data);
    }
  },[data])
    
  return <View style={{width:"100%",display:"flex",flexDirection:"row"}}>
    <Pressable style={[styles.buttonEtats,etat ? {backgroundColor:"rgba(201, 242, 155,1)"} : {backgroundColor:"rgba(236, 217, 221, 1)"} ]}
     onPress={()=>{setEtat(!etat);}} disabled={autotest}>
      <Text>{item}</Text>
    </Pressable> 



    <View style={[{display:"flex",flexDirection:"column",width:"80%"}, !etat && {backgroundColor:"rgba(236, 217, 221, 1)"} ]}>
        <FlatList    
        data={["Vide","0","1"]}
        renderItem={({item,index})=>{
            return      (
            <View style={[{display:"flex",flexDirection:"row",width:"100%"} ]}>
              <View style={[{display:"flex",flexDirection:"column",width:"25%"}, !etat && {backgroundColor:"rgba(236, 217, 221, 1)"} ]}>
                <ValeurLue item={item} etat={etat} data={data1} setData={setData1} 
                dropdown1={dropdownRefList[index*3]} dropdown2={dropdownRefList[index*3+1]} dropdown3={dropdownRefList[index*3+2]} autotest={autotest}/>
              </View>
              <View style={[{display:"flex",flexDirection:"column",width:"25%"},  !etat && {backgroundColor:"rgba(236, 217, 221, 1)"}]}>
                <Dropdown refDropdown={dropdownRefList[index*3]} item ={item} etat={etat} data={data1} 
                setData={setData1} dropdownData={[" ","Vide","0","1"]} toChange={"aEcrire"} nextDropdown={dropdownRefList[index*3+1]} autotest={autotest}/>
              </View>
              <View style={[{display:"flex",flexDirection:"column",width:"25%"}, !etat && {backgroundColor:"rgba(236, 217, 221, 1)"} ]}>
                <Dropdown refDropdown={dropdownRefList[index*3+1]} item={item} etat={etat} data={data1} 
                setData={setData1} dropdownData={[" ","gauche","droite"]} toChange={"deplacement"} nextDropdown={dropdownRefList[index*3+2]} autotest={autotest} />
              </View>
              <View style={[{display:"flex",flexDirection:"column",width:"25%"}, !etat && {backgroundColor:"rgba(236, 217, 221, 1)"} ]}>
                <Dropdown refDropdown={dropdownRefList[index*3+2]} item ={item} etat={etat} data={data1}  
                setData={setData1} dropdownData={[" ","Init","e1","e2","e3","e4","fin"]} toChange={"etatSuivant"} autotest={autotest} />
              </View>
            </View>)
        } }
        keyExtractor={(item, index)=>index} 
        initialNumToRender={3}
        maxToRenderPerBatch={4}
        windowSize={2}
        />
    </View>

   

  </View>
})


export default Ligne;

