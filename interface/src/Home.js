
import { Text, View, useWindowDimensions} from "react-native";
import {  Link } from "react-router-native";

import styles from '../styles';
import {useOrientation} from './useOrientation';

const Home = () => {

const orientation = useOrientation();

const ChangeStyleHomeOrientation = (height) => ( {height:height, borderWidth:3,
  borderRadius:10,
  borderColor:"rgb(8, 143, 143)",    
  marginLeft:"5%",
  marginRight:25, padding:10,
  elevation: 4,
  backgroundColor:"white",
  justifyContent:"center"});

  return (
  <View>

    <View style={orientation==="PORTRAIT" ? ChangeStyleHomeOrientation("82%") :ChangeStyleHomeOrientation("80%") }>
        <Text style={styles.header}>CyAFIS</Text>
        <View style={styles.buttons,orientation==="PORTRAIT" ?{flexDirection:"column"} :{flexDirection:"row"}}>

          <Link to="/newProgram" style={styles.button1}>
              <Text style={styles.textButton}>Ajouter un programme</Text>
          </Link>
          <Link to="/tousPrograms" style={styles.button2}>
              <Text style={styles.textButton2}>Tous les programmes</Text>
          </Link>
        </View>

    </View>
    <View style={{backgroundColor: "rgb(8, 143, 143)",
      height:orientation==="PORTRAIT" ? "3%" : "5%",
      marginRight: 50,
      marginLeft: "10%",
      borderBottomLeftRadius:25,
      borderBottomRightRadius:20,
      shadowColor: 'black',
      shadowOpacity: 0.26,
      shadowOffset: { width: 0, height: 2},
      shadowRadius: 10,
      elevation: 3,
      borderTopWidth:1,
      borderColor:"white"}}>
    </View>
  </View>
)};


export default Home;