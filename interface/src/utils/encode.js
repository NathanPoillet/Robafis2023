
const encodeEcrire=(data)=>{
    switch(data){
      case "Vide": return "V";
      case "1": return "1";
      case "0": return "0";
      default: return "e"
    }
}

const encodeDeplacement = (data) => {
    switch(data){
        case "gauche": return "g";
        case "droite": return "d";
        default: return "e"
    }
}

  const encodeEtatSuivant = (data)=>{
    switch(data){
      case "Init" : return "i"
      case "e1": return "a"
      case "e2": return "b"
      case "e3": return "c"
      case "e4": return "d"
      case "fin": return "f"
      default: return "e"
    }
  }



  const decodeDeplacement = (data) => {
    switch(data){
      case "g": return "gauche";
      case "d": return "droite";
      default: return "e"
    }
  }

  const decodeEtat = (data)=>{
    switch(data){
      case "i" : return "Init"
      case "a": return "e1"
      case "b": return "e2"
      case "c": return "e3"
      case "d": return "e4"
      case "f": return "fin"
      default: return "e"
    }
  }

  const decodeErreurLecture=(data)=>{
    switch(data){
      case "0" : return "Aucun cube n'a été détecté."
      case "h": return "Le robot est hors limite."
      case "2": return "2 cubes ont été détectés."
      case "e": return "Un cube se trouve à cheval sur deux sous-cases."
      default: return "Erreur indéfinie"
    }
  }

  const decodeAbsenceInstruction=(data)=>{
    switch(data){
      case "0" : return "L'écriture à effectuer n'a pas été spécifié pour la valeur lue dans cet état."
      case "1": return "Le déplacement à effectuer n'a pas été spécifié pour la valeur lue dans cet état."
      case "2": return "L'état suivant n'a pas été spécifié pour la valeur lue dans cet état."
      default: return "Erreur indéfinie."
    }
  }

  const encode = (data,etat) => {
    let res="";
    if(data["Vide"]){
      let vide = data["Vide"];
      res= res + etat + "V" + encodeEcrire(vide.aEcrire) + encodeDeplacement(vide.deplacement) + encodeEtatSuivant(vide.etatSuivant) ;
    }

    if(data["0"]){
      let zero = data["0"];
      res= res + etat + "0" + encodeEcrire(zero.aEcrire) + encodeDeplacement(zero.deplacement) + encodeEtatSuivant(zero.etatSuivant) ;
    }

    if(data["1"]){
      let un = data["1"];
      res= res + etat + "1" + encodeEcrire(un.aEcrire) + encodeDeplacement(un.deplacement) + encodeEtatSuivant(un.etatSuivant) ;
    }
    return res;
  }

  export {encode,encodeDeplacement,encodeEcrire,encodeEtatSuivant,decodeAbsenceInstruction,decodeDeplacement,decodeErreurLecture,decodeEtat}