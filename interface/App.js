import { registerRootComponent } from 'expo';
import { Text, View, AppRegistry,PermissionsAndroid, Pressable } from "react-native";
import { NativeRouter, Route, Routes, Link } from "react-router-native";
import { BleManager } from 'react-native-ble-plx'
import {decode as atob, encode as btoa} from 'base-64'
import AnimatedLoader from 'react-native-animated-loader';

import ModifierOuAjouterProgramme from './src/ModifyPage';
import AllProgram from './src/AllProgram';
import BluetoothTest from "./src/bluetoothTest";
import Home from './src/Home';

import styles from './styles';
import { useEffect, useState } from 'react';

import {encode,encodeDeplacement,encodeEcrire,encodeEtatSuivant,decodeAbsenceInstruction,decodeDeplacement,decodeErreurLecture,decodeEtat} from "./src/utils/encode"
import LottieView from "lottie-react-native";
/*


Changer style all programs

*/

const infoColor="green";
const erreurColor="rgb(255,50,50)";

const manager = new BleManager();

const nomDevice=["Makeblock_LE703E9701994E","Makeblock_LE703E979314F7","Makeblock_LE703e97e30197"]

export  default  function App () {


  const serviceUUID="0000ffe1-0000-1000-8000-00805f9b34fb";

  const characteristicWrite_UUID="0000ffe3-0000-1000-8000-00805f9b34fb";
  const characteristicRead_UUID="0000ffe2-0000-1000-8000-00805f9b34fb";

  const [device,setDevice] = useState(null);
  const [message,setMessage]=useState("");
  const [connected,setConnected]=useState(false);
  const [arrayBLE,setArrayBLE] = useState([{text:"test",who:0,color:"rgb(255,50,50)"},{text:"tesdddddddddddd ddddddddddd dddt aaaaaa bbbbbbbb cccc ",who:0},{text:"tesdddddddddddd ddddddddddd ddddddt aaaaaaaaaa bbbbbbbb cccc",who:0,color:"green"}]);

  const [arrayResp,setArrayResp]= useState([]);
  const [resp,setResp]=useState(true);
  const [run,setRun]= useState(false);
  const [programmeEnCours,setProgrammeEnCours]= useState({nom:"autotest",programme:"I205E"});
  const [lastMessage,setLastMessage]=useState("");
  const [etatCourant,setEtatCourant]= useState({etat:"",lu:"",ecrit:"",etatSuivant:""});
  const [interruption,setInterruption]=useState(false);

  const [scanning,setScanning]=useState(false);
  const [menu,setMenu]=useState(0);



  const scanAndConnect=()=> {

    console.log("scanning" + scanning);
    if(device===null && !scanning){
      setScanning(true);
      manager.startDeviceScan(null, {}, (error, device) => {
        if (error) {
          // Handle error (scanning will be stopped automatically)
          console.log("startDeviceScan " + JSON.stringify(error));
          setScanning(false);
          return
        }

        if(device.name!==null) console.log(device.name);

        // Check if it is a device you are looking for based on advertisement data
        // or other criteria.
        if (nomDevice.includes(device?.name)) {
          console.log(" CONNECTED TO " +device.name);

          // Stop scanning as it's not necessary if you are scanning for one device.
          manager.stopDeviceScan();
          setDevice(device);
          setScanning(false);

          // Proceed with connection.
          let i=true;

          device
          .connect()
          .then(device => {
            setConnected(true);
          return device.discoverAllServicesAndCharacteristics()
          }).then(device=>{ 
            // device.services().then(item=>console.log(item.map((item1)=>{if(item1.uuid==="0000ffe1-0000-1000-8000-00805f9b34fb" || item1.uuid==="0000ffe4-0000-1000-8000-00805f9b34fb"  )
            // console.log(item1._manager);})));
            //device.characteristicsForService("0000ffe1-0000-1000-8000-00805f9b34fb").then(i=>console.log(i));
            device.monitorCharacteristicForService(serviceUUID, characteristicRead_UUID, (error,characteristic)=>{
              if(error){
                console.log("monitorCharacteristicForService " +  JSON.stringify(error));
                return
              } else {
                setTimeout(()=>{
                  i = !i;
                  setArrayResp({text:atob(characteristic.value),who:1});
                  setResp(i);
                },100)
              }
            })
          })
          .catch(e=> console.log("monitorCharacteristicForService2 " + JSON.stringify(e)));

          device.onDisconnected((error,device)=>{

            if(error){
              console.log("onDisconnected" + JSON.stringify(error));
            }

            // const arrayTemp = [...arrayBLE];
            // arrayTemp.push({text:"Déconnecté de " + device.name,who:0})
            // setArrayBLE(arrayTemp);


            setScanning(false);

            setConnected(false);
            setDevice(null);

            
            // setTimeout(()=>{
            //   scanAndConnect();
            // },2000)
          })
        }
      })
    }
  }



  const write = async (message)=> {
    console.log(message);
    if(nomDevice.includes(device?.name)){
      setLastMessage(message);
      device?.writeCharacteristicWithResponseForService(serviceUUID,characteristicWrite_UUID,btoa(message))
      .then(res=>console.log("message envoyé : " + message))
      .catch(error=> console.log(error));
    }
  }


  const read = async ()=> {
    if(nomDevice.includes(device?.name)){
      device?.readCharacteristicForService(serviceUUID,characteristicRead_UUID)
      .then(res=>console.log(res))
      .catch(error=> console.log("Lecture " + JSON.stringify(error)));
    }
  }


  const deconnecte =async  ()=>{

    if(device!==null){
      const arrayTemp = [...arrayBLE];
      arrayTemp.push({text:"Déconnecté de " + device.name,who:0})
      setArrayBLE(arrayTemp);

      device.cancelConnection().catch((e)=>{console.log(JSON.stringify(e))})
      setDevice(null);
      setRun(false);
      setInterruption(false);
 
    }

  }



  const decode=(message)=>{

    console.log("message original : " + message);
    message =  message?.replace(/v|!|v!|t/g,"");
   
    let count = 0;
    while(count<=message.length){

      let flag=parseInt(message?.at(count));
      let info = message?.at(count+1);
      let variable = info==="V" ? "Vide" : info;
      count=count+2;


      switch(flag){
        case 0:
          info = parseInt(message?.at(1));
          if(info===0 ){
            setArrayBLE(array=> [...array,{text:": Le message n'a pas été correctement reçu par le robot. Renvoie automatique.",who:1,color:erreurColor}]);
            write(lastMessage);
          } else if(info===1) {
            setArrayBLE(array=> [...array,{text:" : Le message a été reçu par TuringAfis." ,who:1,color:infoColor}]);
            
            try{
              let flagEnvoye = parseInt(lastMessage.at(1));
              console.log("here decode : " + lastMessage);
              if(flagEnvoye===2){
                setRun(true);
              }
            }
            catch(e){
              console.log(e);
            }
          } 
          break;
        case 1:
          setArrayBLE(array=> [...array,{text:"Etat courant : " + decodeEtat(info),who:1,color:infoColor}]);
          setEtatCourant({...etatCourant,etat:decodeEtat(info)});
          break;
  
        case 2:
          setArrayBLE(array=> [...array,{text:"Valeur lue : " + variable,who:1,color:infoColor}]);
          setEtatCourant({...etatCourant,lu:info==="V" ? "Vide" : info});
  
          break;
  
        case 3:
          setArrayBLE(array=> [...array,{text:"Valeur écrite : " + variable,who:1,color:infoColor}]);
          setEtatCourant({...etatCourant,aEcrire:info==="V" ? "Vide" : info});
  
          break;
  
        case 4:
          setArrayBLE(array=> [...array,{text:"Déplacement à effectuer : " + decodeDeplacement(info),who:1,color:infoColor}]);
          setEtatCourant({...etatCourant,etatSuivant:decodeDeplacement(info)});
          break;
  
        case 5:
          setArrayBLE(array=> [...array,{text:": " + decodeErreurLecture(info),who:1,color:erreurColor}]);
          setInterruption(true);
          break;
  
        case 6:
          setArrayBLE(array=> [...array,{text:": Le programme s'est terminé.",who:1,color:infoColor}]);
          setRun(false);
          setInterruption(false);
          break;
  
        case 7:
          setArrayBLE(array=> [...array,{text:": " + decodeAbsenceInstruction(info) ,who:1,color:erreurColor}]);
          setInterruption(true);
          break;
  
        default:
          break;
  
      }

    }

    
    
  }




  useEffect(()=>{

  async function requestBluetoothPermission(){
      try {
        const grantedScan = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.BLUETOOTH_SCAN,
          {
            title: 'Bluetooth Scan Permission',
            message: 'This app needs Bluetooth Scan permission to discover devices.',
            buttonPositive: 'OK',
            buttonNegative: 'Cancel',
          }
        );

        const grantedConnect = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.BLUETOOTH_CONNECT,
          {
            title: 'Bluetooth Connect Permission',
            message: 'This app needs Bluetooth Connect permission to connect to devices.',
            buttonPositive: 'OK',
            buttonNegative: 'Cancel',
          }
        );

        const grantedLocation = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            title: 'Fine Location Permission',
            message: 'This app needs to know location of device.',
            buttonPositive: 'OK',
            buttonNegative: 'Cancel',
          }
        );
        console.log(grantedScan );
        console.log(grantedConnect );
        console.log(grantedLocation );

        if (
          grantedScan === PermissionsAndroid.RESULTS.GRANTED &&
          grantedConnect === PermissionsAndroid.RESULTS.GRANTED &&
          grantedLocation === PermissionsAndroid.RESULTS.GRANTED
        ) {
          console.log('Bluetooth permissions granted');
          // Vous pouvez maintenant commencer la découverte et la connexion Bluetooth ici.
        } else {
          console.log('Bluetooth permissions denied');
        }


      } catch (err) {
        console.warn(err);
      }
    }
    requestBluetoothPermission().then(()=>{
    scanAndConnect();

    }).catch(err=>console.log(err));
    console.log("useffect");
  },[])




  useEffect(()=>{

    if(arrayResp.text){
      decode(arrayResp.text);
      setArrayResp([]);
    }
   
  },[resp])



  useEffect(()=>{
    const arrayTemp = [...arrayBLE];
    console.log(connected)
    if(connected){
      arrayTemp.push({text:"Connecté à " +  device?.name,who:0});
      setArrayBLE(arrayTemp);
    } else {
      arrayTemp.push({text:"Connexion...",who:0})
      setArrayBLE(arrayBLE);
    }
  },[connected])


  const connect = ()=>{

    console.log("connect");
    if(device===null ){

      if(scanning){
        manager.stopDeviceScan();
        setScanning(false);
      } else {
        scanAndConnect();

      }
    }
  }



  return (
  <NativeRouter>
    <View style={styles.container}>
      <View style={styles.nav}>
        <View style={styles.titre}>
          <Text style={styles.titreTxt1} adjustsFontSizeToFit >CyAFIS</Text>
        </View>

        <View style={{width:"84%", flexDirection:"row",alignItems: "center",justifyContent:"center"}}>

        <Link 
        to="/" underlayColor="#f0f4f7" 
        style={[styles.navItem,{width:"25%",height:"80%",backgroundColor: menu===0 ? "rgb(240,240,240)" : "rgb(250,250,250)" }]}
        onPress={()=>{setMenu(0)}}
        disabled={menu===0}
        >
          <Text style={styles.textNav}>Accueil</Text>
        </Link>
        <Link
          to="/tousPrograms"
          underlayColor="#f0f4f7"
          style={[styles.navItem,{width:"25%",height:"80%",backgroundColor: menu===1 ? "rgb(240,240,240)" : "rgb(250,250,250)" }]}
          onPress={()=>{setMenu(1)}}
          disabled={menu===1}
        >
          <Text style={styles.textNav} adjustsFontSizeToFit>Tous les programmes</Text>
        </Link>

        <Link
          to="/TestBluetooth/null"
          underlayColor="#f0f4f7"
          style={[styles.navItem,{width:"25%",height:"80%",backgroundColor: menu===2 ? "rgb(240,240,240)" : "rgb(250,250,250)" }]}
          onPress={()=>{setMenu(2)}}
          disabled={menu===2}
        >
          <Text style={styles.textNav} adjustsFontSizeToFit>Terminal</Text>
        </Link>

        </View>

      </View>

      <View
      style={{position:"absolute",bottom:10,right:10,width:150,borderWidth:1,borderColor:"white",borderRadius:10,height:60,elevation:2,backgroundColor: connected ? "rgb(11, 169, 30)" :  scanning ? "rgba(255, 242, 155,1)" : "rgb(255, 65, 69)",justifyContent:'center',alignItems:"center"}}>
        <Pressable onPress={()=> { connected ? deconnecte() : connect();}}>
          {connected ? <Valide/> : scanning ? <Scan/> :<Text>Déconnecté</Text>}
          {/* <Text style={{color: connected ? "white" : "black"}} >{connected ? "Connecté" : scanning ? "Recherche..." :"Déconnecté"}</Text> */}
          </Pressable>
      </View>
      <Routes>
          <Route exact path="/" element={<Home/>} />
          <Route exact path="/tousPrograms" element={<AllProgram setMenu={setMenu}/>} />
          <Route path="/newProgram" element={<ModifierOuAjouterProgramme setMenu={setMenu}/>}/>
          <Route path="/modifyProgram/:id" element={<ModifierOuAjouterProgramme setMenu={setMenu}/>} />
          <Route path="/TestBluetooth/:nom" element={<BluetoothTest connected={connected} device={device} interruption={interruption} 
          setInterruption={setInterruption} arrayBLE={arrayBLE} setArrayBLE={setArrayBLE} arrayResp={arrayResp} setArrayResp={setArrayResp}
          resp={resp} run={run}  setRun={setRun} programmeEnCours={programmeEnCours} setProgrammeEnCours={setProgrammeEnCours}
          lastMessage={lastMessage} setLastMessage={setLastMessage} setEtatCourant={setEtatCourant} etatCourant={etatCourant} setMenu={setMenu}/>} />

      </Routes>
    </View>
  </NativeRouter>      
 

)};


const Test =() => {
return <Text>salut</Text>
};

const Validation = ()=> <AnimatedLoader
  visible={true}
  source={require("./assets/validation.json")}
  overlayColor="rgba(255,255,255,0.75)"
  animationStyle={{width:100,height:100}}
  speed={1}>
    <Text>Connecté!</Text>
  </AnimatedLoader>
;

const Valide =()=>{

  const [width,setWidth] = useState(50);
  const [marginRight,setMarginRight] = useState(0);


return <View style={{flexDirection:"row", display:"flex",justifyContent:"center",alignItems:"center"}}>
<LottieView
style={{width:width,height:50,marginRight:marginRight}}
source={require('./assets/validation.json')}
autoPlay
loop={false}
speed={1}
onAnimationFinish={()=>{setWidth(0);setMarginRight(0);}}
/>
<Text>Connecté</Text>
</View>};

const Scan =()=><View style={{flexDirection:"row", display:"flex",alignItems:"center"}}>
  <LottieView
  style={{width:30,height:30,marginRight:10}}
  source={require('./assets/bluetooth.json')}
  autoPlay
  loop
  speed={0.5}
  />
<Text>Recherche...</Text>
</View>;



//AppRegistry.registerComponent("MyApp", () => App);
//registerRootComponent(App);
