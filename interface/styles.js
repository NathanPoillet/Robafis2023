import { StyleSheet,Dimensions} from "react-native";



const styles = StyleSheet.create({
    container: {
      paddingTop: 10,
      width:"100%",
      position:"flex"
  
    },
    header: {
      fontSize: 70,
      fontWeight:'bold',
      marginBottom:100
    },
    page: {
      height:"85%",
    },
    nav: {
      display:'flex',
      flexDirection: "row",
      borderBottomWidth:1,
      marginBottom:20,
      height:"10%",  
    },
    navItem: {
      alignItems: "center",
      justifyContent: "center",
      padding: 10,
      width:"33.33333%",
      backgroundColor:"red"
    },
    textNav:{
      fontSize: 15,
      textTransform:"uppercase",
      color:"gray",
    },
    subNavItem: {
      padding: 5
    },
    titre:{
      width:"17%",
      flexDirection:"row",
      alignItems: "center"
    },
    titreTxt1:{
      fontSize: 30,
      marginLeft:10,
      fontWeight:"bold"
    },
    buttons:{
      display:'flex',
      marginTop:50
    },
    button1:{
      alignItems: 'center',
      justifyContent: 'center',
      paddingVertical: 12,
      paddingHorizontal: 32,
      borderRadius: 10,
      elevation: 3,
      backgroundColor: "rgb(8, 143, 143)",
      margin:10,
      height:80,
    },
    button2:{
      alignItems: 'center',
      justifyContent: 'center',
      paddingVertical: 12,
      paddingHorizontal: 32,
      borderRadius: 10,
      elevation: 3,
      backgroundColor: "white",
      borderWidth:2,
      margin:10,
      height:80,
    },
    buttonAcceder:{
      alignItems: 'center',
      justifyContent: 'center',
      paddingVertical: 12,
      paddingHorizontal: 32,
      borderRadius: 10,
      elevation: 3,
      backgroundColor: "rgb(8, 143, 143)",
      margin:10,
      height:80,
      width:"40%",
      borderWidth:1,
      borderColor:"white"

    },
    buttonRun:{
      alignItems: 'center',
      justifyContent: 'center',
      paddingVertical: 12,
      paddingHorizontal: 32,
      borderRadius: 10,
      elevation: 3,
      backgroundColor: "white",
      borderWidth:2,
      margin:10,
      height:80,
      width:"40%"
    },
    buttonEtats:{
      display:"flex",justifyContent:"center",alignItems:"center", width:"15%",
      borderColor:"black",
      borderWidth:1,
      height:120
    },
    textProgramme:{
      fontSize: 15,
      textTransform:"uppercase",
      color:"gray",
      marginLeft:10,
      width:"20%"
    },
    buttonVersOu:{
      display:"flex",
      justifyContent:"center",
      alignItems:"center",
      borderColor:"black",
      borderWidth:1,
      height:40
    },
    textButton:{
      fontSize: 20,
      lineHeight: 21,
      fontWeight: 'bold',
      letterSpacing: 0.25,
      color: 'white',
    },
    textButton2:{
      fontSize: 20,
      lineHeight: 21,
      fontWeight: 'bold',
      letterSpacing: 0.25,
      color: 'black',
    },
    topic: {
      textAlign: "center",
      fontSize: 15
    },
    buttonStyle: {
      width:"100%",
      height:40,

      borderColor:"black",
      borderWidth:1,
    },
  
  });

  export default styles;