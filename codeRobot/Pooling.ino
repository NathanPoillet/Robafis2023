

void stopUrgence(){
  leftMotor.stop();
  rightMotor.stop();
  cartMotor.stop();
  pinceMotor.stop();
}

void reprise(){
  openPince();
  moveCart(INITIAL_DISTANCE,2);
  startProgram(currentProgram);
  
}

int pooling(){
  char cmd[99] = "";
  int code_message = message(cmd);

  if(code_message == 0){
    stopUrgence();
    state=emergencyStop;
    return SUCCESS;
  }else{
    return ERROR;
  }
}
