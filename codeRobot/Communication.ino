
void decode(){
    char cmd[99] = "";
    int code_message;

    if (Serial3.available()){
      code_message = message(cmd);
    } else {
      return;
    }

    switch (code_message) {
      case 0:
        Serial.println("Interruption");
        return;
        // interruption();
      case 1:
        Serial.println("Reprise");
        reprise();
        return;
      case 2:
        Serial.println("Nouveau Programme");
        programme(cmd);
        state = waitAProgramStart;
        startProgram(currentProgram);
        return;
      default:
        encode(FLAG_MSG,MSG_ERROR);
        return;
    }
  return;
}

int message(char* cmd){
  
    char data = Serial3.read();
    if (data == 'I') {
      cmd[0] = data;
      int i=1;
      while (data != 'E') {
        if (Serial3.available()) {
          data = Serial3.read();
          cmd[i] = data;
          i++;
        }
      }
    }

    int code_message;
    int nb_caractere;
    sscanf(cmd+1,"%1d",&code_message);
    sscanf(cmd+2,"%2d",&nb_caractere);

    /* code Message
      0 -> Interruption
      1 -> Reprise
      2 -> Autotest
      3 -> Nouveau Programme
    */

    if (nb_caractere != strlen(cmd)){
      code_message=-1; // Go into the default at the next switch
    } else {
      encode(FLAG_MSG,MSG_OK);
    }

    return code_message;
}

void encode(char flag,char msg) {
  unsigned long time=millis();
  while(time-lastMessage<100) { // Attendre minimum 100ms avant de renvoyer un message
    time = millis();
  }
  Serial3.print(flag);
  Serial3.print(msg);
  lastMessage=millis();
}

void emergencyStopFunction(char flag,char msg){
  state=emergencyStop;
  stopUrgence();
  encode(flag,msg);
}

void initCurrentProgram(){
  for(int i=0;i<5;i++){
    for(int j=0;j<3;j++){
      for(int k=0; k<3; k++){
        currentProgram[i][j][k]=NOT_DETERMINED;
      }
    }
  }
}

void programme(char cmd[99]){

  initCurrentProgram();
  
  int etat;
  int valeur_lue;
  for (int i=4;i<strlen(cmd)-1;i=i+5) {
    etat = convert_etat(cmd[i]); // Etat : indice première liste
    valeur_lue = convert_value(cmd[i+1]); // Valeur lu : indice deuxième liste
    
    currentProgram[etat][valeur_lue][0] = convert_value(cmd[i+2]); // Valeur à écrire
    if (cmd[i+3] == 'g') {
      currentProgram[etat][valeur_lue][1] = 0; // Direction : gauche
    } else if (cmd[i+3] == 'd') {
      currentProgram[etat][valeur_lue][1] = 1; // Direction : droite
    } else {
      currentProgram[etat][valeur_lue][1] = -1; // Erreur
    }
    currentProgram[etat][valeur_lue][2] = convert_etat(cmd[i+4]); // Etat suivant
  }
}

int convert_etat(char cin){
  switch (cin) {
      case 'i':
        return 0;
      case 'a':
        return 1;
      case 'b':
        return 2;
      case 'c':
        return 3;
      case 'd':
        return 4;  
      case 'f':
        return 5;  
      default:
        Serial.print("Erreur Etat");
        return -1;
  }
}

int convert_value(char cin){
  switch (cin){
      case 'V':
        return 0;
      case '0':
        return 1;
      case '1':
        return 2;
      default:
        Serial.print("Erreur Value");
        return -1;
    }
}

int encode_etat(char cin){
  switch (cin) {
      case 0:
        return 'i';
      case 1:
        return 'a';
      case 2:
        return 'b';
      case 3:
        return 'c';
      case 4:
        return 'd';  
      case 5:
        return 'f';  
      default:
        return 'e';
  }
}

int encode_value(char cin){
  switch (cin){
      case 0:
        return 'V';
      case 1:
        return '0';
      case 2:
        return '1';
      default:
        return 'e';
    }
}
