/**
 * @file    testRobot.ino
 * @author  Moeung Alim
 * @version V1.0.0
 * @date    2023/10/24
 * @brief   Description: this file is the first programmation of our robot for robafis 2023.
 *
 */
#include "Constantes.h"

void setup()
{
  Serial.begin(9600);
  Serial3.begin(115200);
  colorsensor0.SensorInit();
  state=waitAProgramStart;
}
int myValue;

void loop() {

  
  decode();

}

int startProgram(int program[5][3][3]){
  openPince();
  int followingDirection=RIGHT;
  int valueToWrite=VALUE_NOTHING;
  int followingState=0;
  int currentMemoryCaseValue=NOT_DEFINE;
  if(state==waitAProgramStart){ // Init values
    currentState=0;
  }
  state=programOn;

  while(currentState != finalState && state==programOn){
    Serial.println("Etat courant:");
    Serial.println(currentState);
    encode(FLAG_STATE,encode_etat(currentState));
    if(pooling()==SUCCESS){
      Serial.println("ARret durgence");
      return ERROR;
    }

    if(readValue(&currentMemoryCaseValue)==ERROR){
      return ERROR;
    }
    

    Serial.println("Valeur lue:");
    Serial.println(currentMemoryCaseValue);
    encode(FLAG_VAL_READ,encode_value(currentMemoryCaseValue));

    valueToWrite=program[currentState][currentMemoryCaseValue][0];
    followingDirection=program[currentState][currentMemoryCaseValue][1];
    followingState=program[currentState][currentMemoryCaseValue][2];
    
    Serial.println("Valeur à écrire:");
    Serial.println(valueToWrite);
    if(valueToWrite==NOT_DETERMINED) {
      emergencyStopFunction(FLAG_LACK_INST,LACK_WRITE);
      return ERROR;
    }
    
    if(pooling()==SUCCESS){
      return ERROR;
    }
    if (writeValue(currentMemoryCaseValue, valueToWrite)==ERROR){
      return ERROR;
    }


    if (followingDirection==NOT_DETERMINED) {
      emergencyStopFunction(FLAG_LACK_INST,LACK_MOVE);
      return ERROR;
    }else{
      if (followingDirection == LEFT)  {
        encode(FLAG_MOVE,'g');
      } else if (followingDirection == RIGHT) {
        encode(FLAG_MOVE,'d');
      } else {
        encode(FLAG_MOVE,'e');
      }
    }
    
    if(pooling()==SUCCESS){
      return ERROR;
    }
    if(changeMemoryCase(followingDirection)==ERROR){
      return ERROR;
    }

    Serial.println("Etat suivant:");
    Serial.println(followingState);
    if (followingState==NOT_DETERMINED){
      emergencyStopFunction(FLAG_LACK_INST,LACK_STATE);
      return ERROR;
    } else {
      currentState=followingState;
    }
  }
  state = waitAProgramStart;
  encode(FLAG_END,' ');
  return SUCCESS;
}

