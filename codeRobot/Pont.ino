int changeMemoryCase(direction leftRight){
  unsigned long timeLastManoeuvre;
  int nbManoeuvre=0;
  Serial.println("Entrée fonction changeMemoryCase");
  do{ 
    if(lineFollower.readSensors()==S1_OUT_S2_OUT || lineFollower.readSensors()==S1_OUT_S2_IN){
      Serial.println("Entrée fonction Manoeuvreee");
      if(leftRight==RIGHT){
        if(millis()-timeLastManoeuvre<750 || nbManoeuvre>3){
          nbManoeuvre++;
          Serial.println("sort des 10 cases mémoires, prend un arret durgence");
          stopUrgence();
          state=emergencyStop;
          return ERROR;
        }
        rightMotor.run(-60);
        leftMotor.run(-70);
        delay(400);
        rightMotor.run(60*1.5);
        leftMotor.stop();
        delay(400);
        rightMotor.stop();
        leftMotor.stop();
        timeLastManoeuvre=millis();
      }else{
        newManoeuvreGauche();
      }
      
    }


    if(leftRight==LEFT){
      leftMotor.run(-65); 
      rightMotor.run(-60); 
    } else {
      leftMotor.run(70); //1.215   -
      rightMotor.run(60);
    }

    //sensorState=S1_IN_S2_OUT;
    while(lineFollower.readSensors()==S1_IN_S2_IN){
      if(pooling()==SUCCESS){
        return ERROR;
      }
    }
  
    while(lineFollower.readSensors()==S1_IN_S2_OUT){ // sensorState!=S1_IN_S2_IN /*|| millis()-startTime<duration*/ 
      if(pooling()==SUCCESS){
        return ERROR;
      }
    }

    //On regarde si ya du noir lorsqu'on va a gauche pour voir si on a lerreur case 1 ->Left
    if(leftRight==LEFT && lineFollower.readSensors()==S1_IN_S2_IN){
      while(lineFollower.readSensors()==S1_IN_S2_IN){
        // if(pooling()==SUCCESS){
        //   return ERROR;
        // }
      }
      leftMotor.stop();
      rightMotor.stop();
      delay(50);
      if(lineFollower.readSensors()==S1_OUT_S2_OUT){
         Serial.println("sort des 10 cases mémoires, prend un arret durgence");
        // emergencyStopFunction(FLAG_ERROR,ERR_OOB);
        stopUrgence();
        state=emergencyStop;
         return ERROR;
      }else{
        leftMotor.run(motorSpeed);
        rightMotor.run(motorSpeed);
      }
      while(lineFollower.readSensors()==S1_IN_S2_OUT){
      }
    }
    
    leftMotor.stop();
    rightMotor.stop();


    
  } while((lineFollower.readSensors()==S1_OUT_S2_OUT || lineFollower.readSensors()==S1_OUT_S2_IN));
  return SUCCESS;
}

void newManoeuvreGauche(){
  rightMotor.run(60);
  leftMotor.run(120);

  delay(600);

  rightMotor.run(-90);
  leftMotor.stop();

  delay(600);
  rightMotor.stop();
  leftMotor.stop();

}