#include "Wire.h"
#include "MeMegaPi.h"
#include <math.h>

#define FLAG_MSG '0'        // Message Réception
#define FLAG_STATE '1'      // Etat actuel
#define FLAG_VAL_READ '2'   // Valeur lue
#define FLAG_VAL_WRITE '3'  // Valeur écrite
#define FLAG_MOVE '4'       // Déplacement
#define FLAG_ERROR '5'      // Erreur Lecture
#define FLAG_END '6'        // Fin Programme
#define FLAG_LACK_INST '7'  // Absence instruction

#define MSG_ERROR '0'         // Erreur Message
#define MSG_OK '1'          // Bien Recu

#define ERR_NO_CUBE '0'     // Pas de cube
#define ERR_2_CUBES '2'     // 2 cubes sur la même case
#define ERR_POSITION 'e'    // Cube entre 2 cases
#define ERR_OOB 'h'         // Robot hors limite
#define ERR_DEFAULT 'd'     // Others 

#define LACK_WRITE '0'      // Pas de valeur a ecrire renseigner
#define LACK_MOVE '1'       // Pas de mouvement renseigner 
#define LACK_STATE '2'      // Pas d'etat suivant renseigner

//#define SIZE_CELL 10        
//#define DISTANCE_BEFORE_CELL 5

#define SUCCESS 1           // Return True
#define UNSUCESS 0          // False
#define ERROR -1            // Error

#define NOT_DETERMINED -1   // None Value
#define INITIAL_DISTANCE 29

//#define DISTANCE_VALUE_0 15 
//#define DISTANCE_VALUE_1 25

/* Définition des ports */
MeUltrasonicSensor ultraSensor(PORT_8); /* Ultrasonic module can ONLY be connected to port 3, 4, 6, 7, 8 of base shield. */
MeColorSensor colorsensor0(PORT_6);
MeLineFollower lineFollower(PORT_7);
MeMegaPiDCMotor leftMotor(PORT2B);
MeMegaPiDCMotor rightMotor(PORT1B);
MeMegaPiDCMotor cartMotor(PORT4B);
MeMegaPiDCMotor pinceMotor(PORT3B);

/* Définition des enum */
//typedef enum {ENTRY,CENTER} where;
typedef enum {VALUE_NOTHING,VALUE_0,VALUE_1,NOT_DEFINE} caseValue;
typedef enum {LEFT,RIGHT} direction;
typedef enum {waitAProgramStart,programOn,emergencyStop} turingAfisState;
//typedef enum{manual,bugDetected} emergencySource;

/* Init variables globales */
int currentState=0;
turingAfisState state=waitAProgramStart;

uint8_t motorSpeed = 50;
uint8_t cartSpeed = 150;
//uint8_t colorResult = 0;
//float distanceMesure=0.0;
int sensorState = lineFollower.readSensors();
const int finalState=5;

float distanceUS(){
  float distance;
  do {
    distance=ultraSensor.distanceCm();
  } while(distance==400.0);
  return distance;
}

int openPince() {
  unsigned long startTime=millis();
  unsigned long duration=1500;
  pinceMotor.run(300);
  while(millis()-startTime<duration){
    //pooling
  }
  pinceMotor.stop();
  return SUCCESS;
}

int closePince() {
  unsigned long startTime=millis();
  unsigned long duration=1500;
  pinceMotor.run(-300);
  while(millis()-startTime<duration){
    //pooling  
  }
  pinceMotor.stop();
  return SUCCESS;
}

int readValue(int* currentMemoryCaseValue){
  *currentMemoryCaseValue=NOT_DEFINE;
  return searchCube(currentMemoryCaseValue); //peut etre emttre &currentMemoryCaseValue
}

int moveCart(float distanceCible,float vitesse){
  float currentDistance=distanceUS();
  //Serial.println(currentDistance);
  unsigned long startTime=millis();
  unsigned long duration=4000;
  
  if (currentDistance>distanceCible) {
    cartMotor.run(-cartSpeed*vitesse);
    while (millis()-startTime<duration && currentDistance>distanceCible) {
      currentDistance=distanceUS();
      //Serial.println(currentDistance);
      //pooling
    }
  } else {
    cartMotor.run(cartSpeed*vitesse);
    while(millis()-startTime<duration && currentDistance<distanceCible){
      currentDistance=distanceUS();
      //Serial.println(currentDistance);
      //pooling
    }
  }

  if (distanceCible==INITIAL_DISTANCE){
    cartMotor.run(cartSpeed*2);
    delay(200);
  }

  //Serial.println(currentDistance);
  cartMotor.stop();
  return SUCCESS;
}

int writeValue(caseValue currentValue, caseValue valueCible){
  if (currentValue != valueCible){
    if(goToCube(currentValue)==ERROR){
      return ERROR;
    }
    //Serial.println("Distance arret");
    //Serial.println(distanceUS());
    Serial.println("Fermeture de la pince");
    if(closePince()==ERROR){
      return ERROR;
    }
    //encode(FLAG_VAL_WRITE,encode_value(valueCible));
    if(goCell(valueCible)==ERROR){
      return ERROR;
    }
    Serial.println("Ouverture pince");
    //Serial.println(distanceUS());
    if(openPince()==ERROR){
      return ERROR;
    }
  }
  else{
    if(moveCart(INITIAL_DISTANCE/2.0,2)==ERROR){
      return ERROR;
    }
    //encode(FLAG_VAL_WRITE,encode_value(valueCible));
  }
  //Serial.println("debut retour case depart");
  if(moveCart(INITIAL_DISTANCE,2)==ERROR){
    return ERROR;
  }
  Serial.println("arrivé case depart");
  //Serial.println(distanceUS());
  //Serial.println("fin retour case depart");
  return SUCCESS;
}

int goToCube(int currentMemoryCaseValue){
  unsigned long startTime;
  if(goCell(currentMemoryCaseValue+3)==ERROR){
    return ERROR;
  }
  if(colorsensor0.ColorIdentify() != RED){
    cartMotor.run(cartSpeed*0.25);
    while(colorsensor0.ColorIdentify() != RED){
      //pooling
    }
    cartMotor.stop();
  }
  return SUCCESS;
}

int searchCube(int* currentMemoryCaseValue){
  unsigned long startTime=millis();
  unsigned long duration=3200;
  float distanceRed=0.0;
  float distance=distanceUS();
  //Serial.println(distance);
  int color=0;
  int nbRed=0;
  float avg=0.0;

  cartMotor.run(-cartSpeed);

  while(millis()-startTime<duration && distance>4){
    //pooling
    color=colorsensor0.ColorIdentify();
    distance=distanceUS();
    //Serial.println(distance);

    if (color==RED){
      nbRed=0;
      do{
        distance=distanceUS(); 
        color=colorsensor0.ColorIdentify();
        Serial.println(distance);
        if (distance<50){
          distanceRed+=distance;
          nbRed=nbRed+1;
        }   
      } while(color==RED && nbRed<5 && distance>4.8);
      //Serial.println("Fin Rouge");
      if(*currentMemoryCaseValue==NOT_DEFINE && nbRed<4){
        avg=(distanceRed/nbRed);
        //distanceMesure=avg;
        Serial.println(nbRed);
        Serial.println("LA MOYENNE LUE EST DE : ");
        Serial.println(avg);
        if (avg>24){
          *currentMemoryCaseValue=VALUE_1;
        } else if(avg>23){
          //emergencyStopFunction(FLAG_ERROR,ERR_POSITION);
          Serial.println("Entre deux cases, error");
        } else if(avg>14){
          *currentMemoryCaseValue=VALUE_0;
        } else if(avg>12){
          //emergencyStopFunction(FLAG_ERROR,ERR_POSITION);
          Serial.println("Entre deux cases, error");
        }else{
          *currentMemoryCaseValue=VALUE_NOTHING;
        }
      } else{
        Serial.println(nbRed);
        Serial.println("Error case plusieurs valeurs, plusieurs cubes sur la même case mémoire");
        //emergencyStopFunction(FLAG_ERROR,ERR_2_CUBES);
        return ERROR;
        //polling erreur=arret durgence
      }
      
    }
  }
  Serial.println(millis()-startTime);
  //Serial.println(avg);
  cartMotor.stop();
  if (*currentMemoryCaseValue==NOT_DEFINE){
    Serial.println("Error, pas de cube sur la case mémoire");
    //emergencyStopFunction(FLAG_ERROR,ERR_NO_CUBE);
    return ERROR;
  }
  return SUCCESS;
}


int goCell(caseValue cell){
  //Serial.println(distanceUS());
  switch(cell){
    case 0:
    return moveCart(8,1);
    //Serial.println(distanceUS());
    case 1:
    return moveCart(17,1);
    //Serial.println(distanceUS());

    case 2:
    return moveCart(26,1);
    //Serial.println(distanceUS());

    case 4:
    return moveCart(11,1.5);

    case 5:
    return moveCart(22.5,1.5);

    default:
    break;
  }
  return SUCCESS;
}




void setup() {
  Serial.begin(9600);
  Serial3.begin(115200);
  colorsensor0.SensorInit();
  state=waitAProgramStart;

}

int nbLecture=0;
int nbEcriture=0;
int nbTour;
int valueToWrite=VALUE_NOTHING;
int currentMemoryCaseValue=NOT_DEFINE;
unsigned long startTime;
unsigned long timeL=0.0;
unsigned long total_timeL=0.0;

void loop() {
  nbTour++;
  startTime=millis();
  if(readValue(&currentMemoryCaseValue)!=ERROR){
    Serial.println("Lecture réussie");
    timeL=millis()-startTime;
    total_timeL+=timeL;
    Serial.println("Valeur lue:");
    Serial.println(currentMemoryCaseValue);
    Serial.println("Temps Lecture:");
    Serial.println(timeL);
    nbLecture++;
  }
moveCart(INITIAL_DISTANCE,2);
  

if(nbTour%10==0){
  Serial.print("Nb tour: ");
  Serial.print(nbTour);
  Serial.print(", NbLectureOk: ");
  Serial.print(nbLecture);
  Serial.println("Temps Lecture moyen:");
  Serial.println(total_timeL/nbTour);
}


}
