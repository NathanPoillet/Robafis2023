#include "Wire.h"
#include "MeMegaPi.h"
#include <math.h>

#define FLAG_MSG '0'        // Message Réception
#define FLAG_STATE '1'      // Etat actuel
#define FLAG_VAL_READ '2'   // Valeur lue
#define FLAG_VAL_WRITE '3'  // Valeur écrite
#define FLAG_MOVE '4'       // Déplacement
#define FLAG_ERROR '5'      // Erreur Lecture
#define FLAG_END '6'        // Fin Programme
#define FLAG_LACK_INST '7'  // Absence instruction

#define MSG_ERROR '0'         // Erreur Message
#define MSG_OK '1'          // Bien Recu

#define ERR_NO_CUBE '0'     // Pas de cube
#define ERR_2_CUBES '2'     // 2 cubes sur la même case
#define ERR_POSITION 'e'    // Cube entre 2 cases
#define ERR_OOB 'h'         // Robot hors limite
#define ERR_DEFAULT 'd'     // Others 

#define LACK_WRITE '0'      // Pas de valeur a ecrire renseigner
#define LACK_MOVE '1'       // Pas de mouvement renseigner 
#define LACK_STATE '2'      // Pas d'etat suivant renseigner

//#define SIZE_CELL 10        
//#define DISTANCE_BEFORE_CELL 5

#define SUCCESS 1           // Return True
#define UNSUCESS 0          // False
#define ERROR -1            // Error

#define NOT_DETERMINED -1   // None Value
#define INITIAL_DISTANCE 29

//#define DISTANCE_VALUE_0 15 
//#define DISTANCE_VALUE_1 25

/* Définition des ports */
MeUltrasonicSensor ultraSensor(PORT_8); /* Ultrasonic module can ONLY be connected to port 3, 4, 6, 7, 8 of base shield. */
MeColorSensor colorsensor0(PORT_6);
MeLineFollower lineFollower(PORT_7);
MeMegaPiDCMotor leftMotor(PORT2B);
MeMegaPiDCMotor rightMotor(PORT1B);
MeMegaPiDCMotor cartMotor(PORT4B);
MeMegaPiDCMotor pinceMotor(PORT3B);

/* Définition des enum */
//typedef enum {ENTRY,CENTER} where;
typedef enum {VALUE_NOTHING,VALUE_0,VALUE_1,NOT_DEFINE} caseValue;
typedef enum {LEFT,RIGHT} direction;
typedef enum {waitAProgramStart,programOn,emergencyStop} turingAfisState;
//typedef enum{manual,bugDetected} emergencySource;

/* Init variables globales */
int currentState=0;
turingAfisState state=waitAProgramStart;

uint8_t motorSpeed = 60;
uint8_t cartSpeed = 150;
//uint8_t colorResult = 0;
//float distanceMesure=0.0;
int sensorState = lineFollower.readSensors();
const int finalState=5;

void stopUrgence(){
  leftMotor.stop();
  rightMotor.stop();
  cartMotor.stop();
  pinceMotor.stop();
}

direction directionChange=-1;
float fois=1.5;


//compte le nombre de fois que l'on teste la manoeuvre d'affilée
int incr=0;

//arrêt du programme en dehors de la grille
bool stop=false;

// dernier temps où on a fait une manoeuvre
unsigned long LastTime = millis();

int temps=400;

direction lastDirection=-1;

const float leftMotorSpeed=70;
const float rightMotorSpeed=60;

int changeMemoryCase(direction leftRight){
  unsigned long timeLastManoeuvre;
  int nbManoeuvre=0;
  Serial.println("Entrée fonction changeMemoryCase");
  do{ 
    if(lineFollower.readSensors()==S1_OUT_S2_OUT || lineFollower.readSensors()==S1_OUT_S2_IN){
      Serial.println("Entrée fonction Manoeuvreee");
      if(leftRight==RIGHT){
        if(millis()-timeLastManoeuvre<750 || nbManoeuvre>3){
          nbManoeuvre++;
          Serial.println("sort des 10 cases mémoires, prend un arret durgence");
          stopUrgence();
          state=emergencyStop;
          return ERROR;
        }
        rightMotor.run(-60);
        leftMotor.run(-70);
        delay(400);
        rightMotor.run(60*1.5);
        leftMotor.stop();
        delay(temps);
        rightMotor.stop();
        leftMotor.stop();
        timeLastManoeuvre=millis();
      }else{
        newManoeuvreGauche();
      }
      
    }


    if(leftRight==LEFT){
      leftMotor.run(-70); 
      rightMotor.run(-60); 
    } else {
      leftMotor.run(70); //1.215   -
      rightMotor.run(60);
    }

    //sensorState=S1_IN_S2_OUT;
    while(lineFollower.readSensors()==S1_IN_S2_IN){
      // if(pooling()==SUCCESS){
      //   return ERROR;
      // }
    }
  
    while(lineFollower.readSensors()==S1_IN_S2_OUT){ // sensorState!=S1_IN_S2_IN /*|| millis()-startTime<duration*/ 
      // if(pooling()==SUCCESS){
      //   return ERROR;
      // }
    }

    //On regarde si ya du noir lorsqu'on va a gauche pour voir si on a lerreur case 1 ->Left
    if(leftRight==LEFT && lineFollower.readSensors()==S1_IN_S2_IN){
      while(lineFollower.readSensors()==S1_IN_S2_IN){
        // if(pooling()==SUCCESS){
        //   return ERROR;
        // }
      }
      leftMotor.stop();
      rightMotor.stop();
      delay(50);
      if(lineFollower.readSensors()==S1_OUT_S2_OUT){
         Serial.println("sort des 10 cases mémoires, prend un arret durgence");
        // emergencyStopFunction(FLAG_ERROR,ERR_OOB);
        stopUrgence();
        state=emergencyStop;
         return ERROR;
      }else{
        leftMotor.run(motorSpeed);
        rightMotor.run(motorSpeed);
      }
      while(lineFollower.readSensors()==S1_IN_S2_OUT){
      }
    }
    
    leftMotor.stop();
    rightMotor.stop();


    
  } while((lineFollower.readSensors()==S1_OUT_S2_OUT || lineFollower.readSensors()==S1_OUT_S2_IN));
  return SUCCESS;
}




void setup() {
  Serial.begin(9600);
  Serial3.begin(115200);
  colorsensor0.SensorInit();
  state=waitAProgramStart;

}


int nbChangement;
unsigned long startTime;
unsigned long timeC=0.0;
unsigned long total_timeC=0.0;
int directionTab[22]={1,1,1,1,1,1,1,1,1,0,0,0,0,0,1,0,1,0,0,1,1,0};

void loop() {
  nbChangement++;
  
  for(int i=0;i<22;i++){
    if(state!=emergencyStop){
      startTime=millis();
      changeMemoryCase((directionTab[i]+0)%2);
      delay(1000);
      timeC=millis()-startTime;
      total_timeC+=timeC;
    }
    
  }
  

if(state==emergencyStop){
   Serial.print("Test échoué");
}
else if(nbChangement%22==0){
  Serial.print("Nb changement: ");
  Serial.print(nbChangement);
  Serial.println("Temps changement moyen:");
  Serial.println(total_timeC/nbChangement);
}


}


void newManoeuvreGauche(){
  rightMotor.run(60);
  leftMotor.run(120);

  delay(600);

  rightMotor.run(-90);
  leftMotor.stop();

  delay(600);
  rightMotor.stop();
  leftMotor.stop();

}
    