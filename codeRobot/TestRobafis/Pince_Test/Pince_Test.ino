#include "Wire.h"
#include "MeMegaPi.h"
#include <math.h>

#define FLAG_MSG '0'        // Message Réception
#define FLAG_STATE '1'      // Etat actuel
#define FLAG_VAL_READ '2'   // Valeur lue
#define FLAG_VAL_WRITE '3'  // Valeur écrite
#define FLAG_MOVE '4'       // Déplacement
#define FLAG_ERROR '5'      // Erreur Lecture
#define FLAG_END '6'        // Fin Programme
#define FLAG_LACK_INST '7'  // Absence instruction

#define MSG_ERROR '0'         // Erreur Message
#define MSG_OK '1'          // Bien Recu

#define ERR_NO_CUBE '0'     // Pas de cube
#define ERR_2_CUBES '2'     // 2 cubes sur la même case
#define ERR_POSITION 'e'    // Cube entre 2 cases
#define ERR_OOB 'h'         // Robot hors limite
#define ERR_DEFAULT 'd'     // Others 

#define LACK_WRITE '0'      // Pas de valeur a ecrire renseigner
#define LACK_MOVE '1'       // Pas de mouvement renseigner 
#define LACK_STATE '2'      // Pas d'etat suivant renseigner

//#define SIZE_CELL 10        
//#define DISTANCE_BEFORE_CELL 5

#define SUCCESS 1           // Return True
#define UNSUCESS 0          // False
#define ERROR -1            // Error

#define NOT_DETERMINED -1   // None Value
#define INITIAL_DISTANCE 29

//#define DISTANCE_VALUE_0 15 
//#define DISTANCE_VALUE_1 25

/* Définition des ports */
MeUltrasonicSensor ultraSensor(PORT_8); /* Ultrasonic module can ONLY be connected to port 3, 4, 6, 7, 8 of base shield. */
MeColorSensor colorsensor0(PORT_6);
MeLineFollower lineFollower(PORT_7);
MeMegaPiDCMotor leftMotor(PORT2B);
MeMegaPiDCMotor rightMotor(PORT1B);
MeMegaPiDCMotor cartMotor(PORT4B);
MeMegaPiDCMotor pinceMotor(PORT3B);

/* Définition des enum */
//typedef enum {ENTRY,CENTER} where;
typedef enum {VALUE_NOTHING,VALUE_0,VALUE_1,NOT_DEFINE} caseValue;
typedef enum {LEFT,RIGHT} direction;
typedef enum {waitAProgramStart,programOn,emergencyStop} turingAfisState;
//typedef enum{manual,bugDetected} emergencySource;

/* Init variables globales */
int currentState=0;
turingAfisState state=waitAProgramStart;

uint8_t motorSpeed = 50;
uint8_t cartSpeed = 150;
//uint8_t colorResult = 0;
//float distanceMesure=0.0;
int sensorState = lineFollower.readSensors();
const int finalState=5;


float distanceUS(){
  float distance;
  do {
    distance=ultraSensor.distanceCm();
  } while(distance==400.0);
  return distance;
}

int openPince() {
  Serial.println("ouverture pince");
  unsigned long startTime=millis();
  unsigned long duration=1500;
  pinceMotor.run(300);
  while(millis()-startTime<duration){
    //pooling
  }
  pinceMotor.stop();
  return SUCCESS;
}

int closePince() {
  Serial.println("Close pince");
  unsigned long startTime=millis();
  unsigned long duration=1500;
  pinceMotor.run(-300);
  while(millis()-startTime<duration){
    //pooling  
  }
  pinceMotor.stop();
  return SUCCESS;
}



void setup() {
  Serial.begin(9600);
  Serial3.begin(115200);
  colorsensor0.SensorInit();
  state=waitAProgramStart;

}



void loop() {
  //pinceMotor.run(-300);
  //closePince();
  openPince();

  

}

