float distanceUS(){
  float distance;
  do {
    distance=ultraSensor.distanceCm();
  } while(distance==400.0);
  return distance;
}

int openPince() {
  unsigned long startTime=millis();
  unsigned long duration=1500;
  pinceMotor.run(-300);
  while(millis()-startTime<duration){
    if(pooling()==SUCCESS){
      return ERROR;
    }
  }
  pinceMotor.stop();
  return SUCCESS;
}

int closePince() {
  unsigned long startTime=millis();
  unsigned long duration=1500;
  pinceMotor.run(300);
  while(millis()-startTime<duration){
    if(pooling()==SUCCESS){
      return ERROR;
    }   
  }
  pinceMotor.stop();
  return SUCCESS;
}

int readValue(int* currentMemoryCaseValue){
  *currentMemoryCaseValue=NOT_DEFINE;
  return searchCube(currentMemoryCaseValue); //peut etre emttre &currentMemoryCaseValue
}

int moveCart(float distanceCible,float vitesse){
  float currentDistance=distanceUS();
  //Serial.println(currentDistance);
  unsigned long startTime=millis();
  unsigned long duration=4000;
  
  if (currentDistance>distanceCible) {
    cartMotor.run(-cartSpeed*vitesse);
    while (millis()-startTime<duration && currentDistance>distanceCible) {
      currentDistance=distanceUS();
      //Serial.println(currentDistance);
      if(pooling()==SUCCESS){
      return ERROR;
      }
    }
  } else {
    cartMotor.run(cartSpeed*vitesse);
    while(millis()-startTime<duration && currentDistance<distanceCible){
      currentDistance=distanceUS();
      //Serial.println(currentDistance);
      if(pooling()==SUCCESS){
      return ERROR;
      }
    }
  }

  if (distanceCible==INITIAL_DISTANCE){
    cartMotor.run(cartSpeed*2);
    delay(200);
  }

  Serial.println(currentDistance);
  cartMotor.stop();
  return SUCCESS;
}

int writeValue(caseValue currentValue, caseValue valueCible){
  if (currentValue != valueCible){
    if(goToCube(currentValue)==ERROR){
      return ERROR;
    }
    //Serial.println("Distance arret");
    //Serial.println(distanceUS());
    Serial.println("Fermeture de la pince");
    if(closePince()==ERROR){
      return ERROR;
    }
    encode(FLAG_VAL_WRITE,encode_value(valueCible));
    if(goCell(valueCible)==ERROR){
      return ERROR;
    }
    Serial.println("Ouverture pince");
    Serial.println(distanceUS());
    if(openPince()==ERROR){
      return ERROR;
    }
  }
  else{
    if(moveCart(INITIAL_DISTANCE/2.0,2)==ERROR){
      return ERROR;
    }
    encode(FLAG_VAL_WRITE,encode_value(valueCible));
  }
  //Serial.println("debut retour case depart");
  if(moveCart(INITIAL_DISTANCE,2)==ERROR){
    return ERROR;
  }
  Serial.println("arrivé case depart");
  Serial.println(distanceUS());
  //Serial.println("fin retour case depart");
  return SUCCESS;
}

int goToCube(int currentMemoryCaseValue){
  unsigned long startTime;
  if(goCell(currentMemoryCaseValue+3)==ERROR){
    return ERROR;
  }
  if(colorsensor0.ColorIdentify() != RED){
    cartMotor.run(cartSpeed*0.25);
    while(colorsensor0.ColorIdentify() != RED){
      if(pooling()==SUCCESS){
        return ERROR;
      }
    }
    cartMotor.stop();
  }
  return SUCCESS;
}

int searchCube(int* currentMemoryCaseValue){
  unsigned long startTime=millis();
  unsigned long duration=3200;
  float distanceRed=0.0;
  float distance=distanceUS();
  //Serial.println(distance);
  int color=0;
  int nbRed=0;
  float avg=0.0;

  cartMotor.run(-cartSpeed);

  while(millis()-startTime<duration && distance>5){
    if(pooling()==SUCCESS){
      return ERROR;
    }
    color=colorsensor0.ColorIdentify();
    distance=distanceUS();
    //Serial.println(distance);

    if (color==RED){
      nbRed=0;
      do{
        distance=distanceUS(); 
        color=colorsensor0.ColorIdentify();
        Serial.println(distance);
        if (distance<50){
          distanceRed+=distance;
          nbRed=nbRed+1;
        }   
      } while(color==RED && nbRed<5 && distance>5);
      //Serial.println("Fin Rouge");
      if(*currentMemoryCaseValue==NOT_DEFINE && nbRed<4){
        avg=(distanceRed/nbRed);
        //distanceMesure=avg;
        Serial.println(nbRed);
        Serial.println(avg);
        if (avg>24){
          *currentMemoryCaseValue=VALUE_1;
        } else if(avg>21.5){
          emergencyStopFunction(FLAG_ERROR,ERR_POSITION);
        } else if(avg>12.5){
          *currentMemoryCaseValue=VALUE_0;
        } else if(avg>10.75){
          emergencyStopFunction(FLAG_ERROR,ERR_POSITION);
        }else{
          *currentMemoryCaseValue=VALUE_NOTHING;
        }
      } else{
        Serial.println(nbRed);
        Serial.println("Error case plusieurs valeurs, plusieurs cubes sur la même case mémoire");
        emergencyStopFunction(FLAG_ERROR,ERR_2_CUBES);
        return ERROR;
        //polling erreur=arret durgence
      }
      
    }
  }
  Serial.println(millis()-startTime);
  //Serial.println(avg);
  cartMotor.stop();
  if (*currentMemoryCaseValue==NOT_DEFINE){
    Serial.println("Error, pas de cube sur la case mémoire");
    emergencyStopFunction(FLAG_ERROR,ERR_NO_CUBE);
    return ERROR;
  }
  return SUCCESS;
}


int goCell(caseValue cell){
  //Serial.println(distanceUS());
  switch(cell){
    case 0:
    return moveCart(8,1);
    //Serial.println(distanceUS());
    case 1:
    return moveCart(17,1);
    //Serial.println(distanceUS());

    case 2:
    return moveCart(26,1);
    //Serial.println(distanceUS());

    case 4:
    return moveCart(11,1.5);

    case 5:
    return moveCart(22.5,1.5);

    default:
    break;
  }
  return SUCCESS;
}